

translate([-16,0,3])
charniere();
translate([16,0,3])
rotate([0, 0, 180])
charniere();


module charniere() {
    difference() {  
        union() {
            cylinder(h=6, r=10, center=true, $fn=100);
            translate([16,0,0])
                rotate([90, 0, 0])
                cylinder(h=30, r=3, center=true, $fn=100);
            translate([8.5,-2.5,0])
                cube([16,25,6], center=true);    
        }
        cylinder(h=100, r=5, center=true, $fn=100);
        translate([0,15,0])
            cylinder(h=100, r=5, center=true, $fn=100);
        translate([0,-15,0])
            cylinder(h=100, r=5, center=true, $fn=100);
        translate([16,-7.5,0])
            cube([9, 5.4, 100], center=true);    
        translate([16,2.5,0])
            cube([9, 5.4, 100], center=true);    
        translate([16,12.5,0])
            cube([9, 5.4, 100], center=true);    
        translate([16,0,0])
            rotate([90, 0, 0])
            cylinder(h=100, r=1.1, center=true, $fn=100);
    }
}

