

translate([-10,-10,0])
axe();
translate([10,-10,0])
axe();
translate([-10,10,0])
axe();
translate([10,10,0])
axe();

module axe() {
    difference() {  
        union() {
            translate([0,0,22])
                cylinder(h=3, r1=6, r2=5, center=true, $fn=100);
            translate([0,0,20])
                cylinder(h=1, r1=5, r2=6, center=true, $fn=100);
            translate([0,0,11.5])
                cylinder(h=23, r=5, center=true, $fn=100);
            translate([0,0,1])
                cylinder(h=2, r=7, center=true, $fn=100);
        }
        translate([0,0,53])
        cylinder(h=100, r=2.5, center=true, $fn=100);
        rotate([0,0,120])
            translate([0,5,53])
            cube([2, 10, 100], center=true);    
        rotate([0,0,240])
            translate([0,5,53])
            cube([2, 10, 100], center=true);    
        translate([0,5,53])
            cube([2, 10, 100], center=true);    
    }
}