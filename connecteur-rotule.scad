

$tolerance=0.4/2;  
$angle=40;

coupe();
rotate([0, 0, 180])
tete();
//translate([16,0,3])
//charniere();


module coupe() {
    union() {
        difference() {  
            union () {
                translate([-20,0,0])
                difference() {  
                    union() {
                        cylinder(h=6, r=10, center=true, $fn=100);
                        //translate([16,0,0])
                        //    rotate([90, 0, 0])
                        //    cylinder(h=30, r=3, center=true, $fn=100);
                        translate([10,0,0])
                            cube([20,20,6], center=true);    
                    }
                    cylinder(h=100, r=5, center=true, $fn=100);
                }
                sphere(r=10, $fn=100);
            }
            sphere(r=7+$tolerance, $fn=100);
            translate([4,0,0])
                rotate([0, $angle, 0])
                translate([50,0,0])
                cube([100,100,100], center=true);    
        }
    }
}

module tete() {
    union() {
        translate([-20,0,0])
        difference() {  
            cylinder(h=6, r=10, center=true, $fn=100);
            cylinder(h=100, r=5, center=true, $fn=100);
        }
        sphere(r=7-$tolerance, $fn=100);
        translate([-6,0,0])
        rotate([0, 90, 0])
        cylinder(h=10, r=3, center=true, $fn=100);
    }
}
