

translate([11,12,3])
embout_large();
translate([-26,12,3])
embout_large();
translate([11,-12,3])
embout_large();
translate([-26,-12,3])
embout_large();



module embout_large() {
    difference() {  
        union() {
            translate([0,0,3])
                cylinder(h=6, r=10, center=true, $fn=100);
            translate([10,0,9])
                rotate([90, 0, 90])
                cylinder(h=30, r=6, center=true, $fn=100);
                //cube([20,8.8,8.8], center=true);    
            translate([10,0,3])
                cube([30,5,6], center=true);    
        }
        translate([0,0,7.5])
            cylinder(h=3.01, r1=10, r2=15, center=true, $fn=100);
        cylinder(h=100, r=5, center=true, $fn=100);
        translate([59.5,0,9])
            rotate([90, 0, 90])
            cylinder(h=100, r=3.1 , center=true, $fn=100);
        translate([0,0,59])
            cylinder(h=100, r=15, center=true, $fn=100);
    }
}

module embout_fin() {
    difference() {  
        union() {
            translate([0,0,3])
                cylinder(h=6, r=10, center=true, $fn=100);
            translate([10,0,4.5])
                rotate([90, 0, 90])
                cylinder(h=20, r=4.5, center=true, $fn=100);
                //cube([20,8.8,8.8], center=true);    
        }
        translate([0,0,7.5])
            cylinder(h=3.01, r1=10, r2=13, center=true, $fn=100);
        cylinder(h=100, r=5, center=true, $fn=100);
        translate([59.5,0,4.5])
            rotate([90, 0, 90])
            cylinder(h=100, r=2.5, center=true, $fn=100);
        translate([0,0,59])
            cylinder(h=100, r=13, center=true, $fn=100);
    }
}

